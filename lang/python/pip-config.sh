#!/bin/bash -e

declare -A configs=(
    ['global.timeout']='600'
    ['global.retries']='10'
    ['global.index-url']='https://nexus.coderdev.com/repository/pypi/simple/'
    ['global.trusted-host']='nexus.coderdev.com'
)
for key in "${!configs[@]}"; do
    pip config set "$key" "${configs[$key]}"
done
