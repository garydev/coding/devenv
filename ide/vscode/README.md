# VS Code Extensions

## General Extensions

- **IntelliCode**
  provides AI-assisted development features for Python, TypeScript/JavaScript and Java developers,
  with insights based on understanding your code context combined with machine learning.

- **IntelliCode Completions**
  predicts up to a whole line of code based on your current context. Predictions appear as 
  grey-text to the right of your cursor.

  > This extension supports Python, JavaScript, and TypeScript, and is automatically installed as
  > a part of the main IntelliCode extension.

- **Better Comments**

  You can custom the format of customized comments types in `settings.json` (see the [configuration template](./vscode-settings/better-comments.json)).

  > The settings above will replace all the default settings of this extension.
  > If you only customized some of the built-in types, you still need to copy all
  > configurations from the extension's introduction page. Otherwise, you will loss
  > the default configurations for other built-in comment types.

  Other characters may be used for special comments include: `#:`, `#%`, `#|`, `#@`, `#{...}`, `#[...]`(`]`/`}` is not necessary), .

- **Color Highlight**
  Highlight HEX numbered web colors in your editor.

- **Hex Editor**

## Python Extension Pack

- Python
- Formatter
  - Black Formatter
  - autopep8
  - Ruff
- Linter
  - Pylance
  - Ruff
- Python Docstring Highlighter
- Jupyter
- Python Environment Manager
- PyPI Assistant
- isort: auto fix import problems
- autoDocstring
- Django
- Jinja

## Shell Extension Pack

- Bash IDE

  > utilizing the **Bash Language Server** and integrates with `explainshell` and `shellcheck`.

- shell-format
- Better Shell Syntax
- ShellCheck

  configuration file: `~/.shellcheckrc`.

  ```shell
  external-sources=true  # let shellcheck access arbitrary files,
                         # whether or not they're specified as input.
  ```

  > corresponding to CLI's argument `-x,--external-sources`.

  Give ShellCheck a path in which to search for sourced files:

  ```shell
  # shellcheck source-path=src/examples:SCRIPTDIR/../libs
  ```

  > corresponding to CLI's argument `-P,--source-path=SOURCEPATHS`, seprate path 
  > with `:` on Linux.

  Tell ShellCheck where to find a sourced file with absolute or relative path:

  ```shell
  # shellcheck source=src/examples/config.sh
  ```

  > corresponding to CLI's positional arguments.
  
- Bash Debug
  
  > A bash debugger GUI frontend based on awesome `bashdb` scripts (**`bashdb` now included in**
  > **this package**).

- **Bats**
  language support of `bats` testing framework for bash shell script.

- shellman
- **Shebang Snippets**: provides auto-complete of shebang line for script languages.
- PowerShell
- vscode-nushell-lang

## Java Extension Pack

- Language Support for Java(TM) by Red Hat
- Debugger for Java
- Test Runner for Java
- Dependency Manager
  - Maven for Java
  - Gradle for Java
- Gradle Language Support
- Project Manager for Java

### XML

- XML Tools
- XML

### Extra Extension for SpringBoot

- Spring Initializr Java Support
- Spring Boot Tools
- Spring Boot Dashboard

## C/C++ Extension Pack

- C/C++
- C/C++ Themes
- Better C++ Syntax

### Make Tool

- Makefile Tools
- CMake
- CMake Tools

## Rust Extension Pack

- **rust-analyzer**: provides Rust programming language.

  > It is recommended over and replaces `rust-lang.rust`.

- **Dependi**: supports multiple languages including **Rust**, Go, JavaScript, TypeScript, Python and PHP.

  > crates (`serayuzgur.crates`) is now replaced by Dependi.

- **Even Better TOML**

## C#.NET Extension Pack

- **C# Dev Kit**
  helps you manage your code with a solution explorer and test your code with integrated unit test
  discovery and execution.
  This extension builds on top of C# language capabilities provided by the C# extension and 
  enhances your C# environment by adding a set of powerful tools and utilities that integrate 
  natively with VS Code to help C# developers write, debug, and maintain their code faster and with
  fewer errors.
- C#
- **IntelliCode for C# Dev Kit**
  provides IntelliCode member ranking & whole-line autocomplete for C# Dev Kit users.
- .NET MAUI
- Polyglot Notebooks

## Web Development Extension Pack

- Prettier
- TypeScript Hero
- ESLint
- npm Intellisense
- JavaScript (ES6) code snippets
- Search node_modules

### HTML

- HTML CSS Support
- IntelliSense for CSS class names in HTML
- Tailwind CSS IntelliSense

### Framework

- React
  - Reactjs code snippets
  - React Native Tools
- Angular
  - Angular Language Service
  - Angular-cli
  - Angular Schematics
  - SimonTest: Analyzes your Angular code and generates tests.
  - Angular Snippets (Version 13)
  - Angular Material 2, Flex layout 1,Covalent 1 & Material icon snippets
- Vue
  - Vetur
  - Vue Language Features (Volar)
  - TypeScript Vue Plugin (Volar)
  - Vue VSCode Snippets

### Web Server

- NGINX Configuration Language Support
  
  > **NGINX Configuration Language Support** is prefered, as **NGINX Configuration** has not been
  > updated since `2019/10/22`.

- Caddyfile Support

## Version Control Extension Pack

- GitLens — Git supercharged
- Git History
- Git Graph
- Partial Diff

## Remote Development Extension Pack

- WSL
- Remote - SSH
- Remote - SSH: Editing Configuration Files
- Remote Explorer
- Remote - Tunnels
- Remote Repositories

### DevOps Extension Pack

- Docker
- Dev Containers
- Kubernetes
- HashiCorp Terraform

## Markdown Extension Pack

- markdownlint
- **Markdown All in One**: enhanced Markdown editing support to VS Code.
- **:emojisense:**: Adds suggestions and autocomplete for emoji.

### Markdown Preview

- [x] Markdown Preview Enhanced
- Built-in
  - Markdown+Math
  - Markdown Checkboxes
  - Markdown Preview Mermaid Support
  - **Markdown Preview Github Styling**: extends VS Code's built-in markdown preview with GitHub style.
  - Markdown Preview VS Code Highlighting
  - **Markdown Emoji**: Adds `:emoji:` syntax support to built-in Markdown preview.
  - Markdown Footnotes

## Database Extension Pack

- MySQL Shell for VS Code